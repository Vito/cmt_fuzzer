Usage
=====



Server
------

The server can be started in Mode 1 or Mode 2 via environment variable ** TEST_APP_MODE **.

The server reads .csv data from standard input.

To start the server with a file  market_data.csv on default port in specified mode:

** env TEST_APP_MODE=1 ./bin/testApp < market_data.csv **

Program options:

 * ** --help **                :print help
 * ** --port arg (=27500) **   :listener tcp port
 * ** --j arg  **              :Number of worker threads


Clients
------

There are 2 clients: a C++ and a python client.

Building instructions for server and c++ client:
------------------------------------------------

The project uses cmake and requires Boost (Version 1.71 assumed).
Should also work with newer and probably some older, but not too old versions. 

If other boost version is to be used, this should be configured in src/CMakeLists.txt.

Simple building:

** mkdir build && cd build **

** cmake .. **

The binary files are stored  in ** build/bin** subdirectory.

Other options:

For the case conan package manager is present **-DUSE_CONAN=ON** option can be specified:

** mkdir build && cd buld **

** conan install  .. **

** cmake -DUSE_CONAN=ON .. **

Other cmake options:

For building in Debug / Release mode, **-DCMAKE_BUILD_TYPE** can be set to Debug or Release.

For the case that older cmake version is present, the generation of precompiled header can be turn off via: ** -DUSE_PCH=OFF **

For printing some more Debuging information: ** -DUSE_DEBUG=ON ** can be specified.


C++ client
----------

The C++ client can only be signed with one symbol.

Program options:

 * ** --help     **              :print help
 * ** --host arg (=localhost) ** :server address
 * ** --port arg (=27500) **     :server port
 * ** --symbol arg **            :symbol, for ex. AAL

For example to sign a client for AAL symbol:

** ./bin/client --symbol AAL **

Python client:
--------------

Python client is tested with python 3.9.0. 

Python client provides simple GUI and the assigned symbol can be changed.

Start via: ** python client.py **

** Requirements:** python with **tkinter** support and **PySimpleGUI** package.

The package can be installed via python package manager

** pip install PySimpleGUI ** 

