#######################################################################
#
# File: client.py

# Daniel Petrovic , Dec/2020
#
# Simple client for receiving updates from testApp server
# 
# Requirements: PySimpleGUI
# Install via: pip install PySimpleGUI
#
# Usage:    python client.py --host HOST --port PORT
# Example:  python client.py --host 127.0.0.1 --port 27500
# Defaults: HOST=localhost, PORT=27500
#######################################################################

import argparse
import PySimpleGUI as gui
import time
import threading, queue
import socket

parser = argparse.ArgumentParser(description='Simple client for testApp server')
parser.add_argument('--host', nargs='?', type=str, default='localhost')
parser.add_argument('--port', nargs='?', type=int, default=27500)
args = parser.parse_args()

done = True
symbol = ''

def do_update_window(w, q):
    while True:
        if done:
            while not q.empty():
                q.get()
            break
        if not q.empty():
            data = q.get()
            w.write_event_value('-ready-', data)
        time.sleep(0.01)

def do_request(q):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.connect((args.host, args.port))
    msg = "00:00:00.000000 %s 0.0000\n" % symbol
    s.sendall(msg.encode('ascii'));

    data = ''
    while (True):
        if done:
            data = ''
            break
        data = data + s.recv(1024).decode('ascii')
        if len(data) > 0:
            pos = data.rfind('\n')
            q.put(data[:pos])
            data = data[pos+1:]

def create_window():
    layout = [ [ gui.Text('Enter symbol:')],
               [ gui.Input(size=(10, 50), key='-in-')]
                + [ gui.Button('Send') ]
                + [ gui.Button('Cancel') ] 
                + [ gui.Button('Clear') ],
               [ gui.Output(size=(80,30), key='-out-')] ]

    w = gui.Window('Client', layout)
    return w


def start_updater(w, q):
    threading.Thread(target=do_update_window, args=(w,q,), daemon=True).start()

def start_request(q):
    threading.Thread(target=do_request, args=(q,), daemon=True).start()

window = create_window()
data_queue = queue.Queue()

while True:

    event, values = window.read()

    if event == gui.WIN_CLOSED:
        break

    elif event == 'Send':
        done = False
        symbol = values['-in-']
        start_request(data_queue)
        start_updater(window, data_queue)

    elif event == 'Cancel':
        done = True

    elif event == 'Clear':
        window['-out-'].update('')

    elif event == '-ready-':
        print(values['-ready-'])

window.close()
