/////////////////////////////////////////////////////////////////////////////////
//
//  Daniel Petrovic, Dec/2020
//
//  File: utility.hpp
//
//  Description:
//
//  Useful utility functions and macros
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef TEST_UTILITY_HPP_
#define TEST_UTILITY_HPP_

#include <array>
#include <chrono>
#include <iostream>
#include <string>

// simple primitive logging, not synchronized
#ifdef USE_DEBUG
#define Debug(M, ...)                                                          \
    fprintf(stderr, "[DEBUG] %s:%d:%s: " M "\n", __FILE__, __LINE__, __func__, \
            ##__VA_ARGS__)
#else
#define Debug(M, ...) ((void)0)
#endif

#define LogErr(M, ...)                                                         \
    fprintf(stderr, "[ERROR] %s:%d:%s: " M "\n", __FILE__, __LINE__, __func__, \
            ##__VA_ARGS__)
#define LogInfo(M, ...)                                                       \
    fprintf(stdout, "[INFO] %s:%d:%s: " M "\n", __FILE__, __LINE__, __func__, \
            ##__VA_ARGS__)

#define CONCAT_STR(s1, s2) CONCAT_STR_IMPL(s1, s2)
#define CONCAT_STR_IMPL(s1, s2) s1##s2

#define ANONYM_VAR(v) CONCAT_STR(v, __LINE__)

#define SCOPE_EXIT \
    auto ANONYM_VAR(SCOPE_EXIT_VAR) = ::test::detail::ScopeQuardOnExit() + [&]()

namespace test
{
namespace detail
{
enum class ScopeQuardOnExit
{
};

template <typename F>
struct ScopeExit final
{
    ScopeExit(F&& f) : f_{std::forward<F>(f)} {}
    ~ScopeExit() { f_(); }
    F f_;
};

template <typename Fun>
ScopeExit<Fun> operator+(ScopeQuardOnExit, Fun&& fn)
{
    return ScopeExit<Fun>(std::forward<Fun>(fn));
}

}  // namespace detail

std::string asString(const std::chrono::system_clock::time_point& tp);

template <typename Handle>
bool setThreadName(Handle handle, const std::string& new_name)
{
    std::array<char, 64> old_name;

    if (pthread_getname_np(handle, &old_name[0], old_name.size()) != 0)
    {
        std::cerr << "Could not read thread name\n";
        return false;
    }

    bool ok = pthread_setname_np(handle, new_name.c_str()) == 0;

    if (ok)
    {
        std::cout << "Renamed thread from " << old_name.data() << " to "
                  << new_name << std::endl;
    }
    else
    {
        std::cerr << "Could not rename thread\n";
    }

    return ok;
}

}  // namespace test

#endif
