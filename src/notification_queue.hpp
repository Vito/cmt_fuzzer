/////////////////////////////////////////////////////////////////////////////////
//
//  Daniel Petrovic, Dec/2020
//
//  File: notification_queue.hpp
//
//  Description:
//
//  Thread-safe queue
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef TEST_NOTIFICATION_QUEUE_HPP_
#define TEST_NOTIFICATION_QUEUE_HPP_

#include <deque>
#include <mutex>

namespace test
{
template <typename T>
class NotifcationQueue
{
    using Lock = std::unique_lock<std::mutex>;

   public:
    //
    // Blocking push
    void push(const T&);
    
    // 
    // Non-blocking push
    bool tryPush(const T&);

    //
    // Blocking pop
    bool pop(T&);

    //
    // Non-blocking pop
    bool tryPop(T&);

    //
    // Signal if no more work should be done
    // Always acquires a lock
    void done();

   private:
    std::deque<T> q_;
    std::mutex mtx_;
    std::condition_variable ready_;
    bool done_{false};
};

template <typename T>
void NotifcationQueue<T>::done()
{
    {
        Lock lck{mtx_};
        done_ = true;
    }
    ready_.notify_all();
}

template <typename T>
void NotifcationQueue<T>::push(const T& t)
{
    {
        Lock lck{mtx_};
        q_.push_back(t);
    }
    ready_.notify_one();
}

template <typename T>
bool NotifcationQueue<T>::tryPush(const T& t)
{
    {
        Lock lck{mtx_, std::try_to_lock};
        if (!lck) return false;
        q_.push_back(t);
    }
    ready_.notify_one();
    return true;
}

template <typename T>
bool NotifcationQueue<T>::pop(T& t)
{
    Lock lck{mtx_};
    while (q_.empty() && !done_) ready_.wait(lck);
    if (done_ || q_.empty()) return false;
    t = q_.front();
    q_.pop_front();
    return true;
}

template <typename T>
bool NotifcationQueue<T>::tryPop(T& t)
{
    Lock lck{mtx_, std::try_to_lock};
    if (!lck || q_.empty()) return false;
    t = q_.front();
    q_.pop_front();
    return true;
}

}  // namespace test

#endif
