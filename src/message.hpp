/////////////////////////////////////////////////////////////////////////////////
//
//  Daniel Petrovic, Dec/2020
//
//  File: message.hpp
//
//  Description:
//
//  Data structure for de-/serializing and sending via tcp.
//  Simple wrapper around DataRow
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef TEST_MESSAGE_HPP_
#define TEST_MESSAGE_HPP_

#include "table.hpp"

namespace test
{
struct Message
{
    DataRow data;
};

std::ostream& operator<<(std::ostream&, const Message&);
std::istream& operator>>(std::istream&, Message&);

}  // namespace test

#endif
