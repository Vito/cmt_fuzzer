/////////////////////////////////////////////////////////////////////////////////
//
//  Daniel Petrovic, Dec/2020
//
//  File: tcp.hpp
//
//  Description:
//
//  Classes for handling tcp connection: Session, TcpServer
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef TEST_SESSION_HPP_
#define TEST_SESSION_HPP_

#include "message.hpp"
#include "table.hpp"
#include "utility.hpp"

#include <chrono>
#include <memory>
#include <sstream>

#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/asio/buffers_iterator.hpp>
#include <boost/asio/io_context.hpp>
#include <boost/asio/placeholders.hpp>
#include <boost/asio/read.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/system_context.hpp>
#include <boost/system/error_code.hpp>

namespace test
{
class Session : public std::enable_shared_from_this<Session>
{
    using tcp = boost::asio::ip::tcp;
    using ChronoClock = std::chrono::system_clock;
    using ChronoTimepoint = ChronoClock::time_point;

public:
    template <typename SignCallback, typename ReleaseCallback>
    Session(boost::asio::io_context& ioc, SignCallback&& cbIn,
            ReleaseCallback&& cbOut)
        : socket_{ioc},
          sign_callback_(std::forward<SignCallback>(cbIn)),
          release_callback_(std::forward<ReleaseCallback>(cbOut))
    {
    }

    auto& socket() { return socket_; }

    const std::string& symbol() const noexcept { return symbol_; }

    ChronoTimepoint loginTime() const noexcept { return login_time_; }

    void start(); 
    void send(const Message&);

    // Synchron write
    template <typename Buffer>
    unsigned sendSynch(Buffer&& buf, boost::system::error_code& ec)
    {
        return boost::asio::write(
            socket_, boost::asio::buffer(std::forward<Buffer>(buf)), ec);
    }

private:
    //
    // Async read
    void startRead();
    void onRead(const boost::system::error_code& ec, unsigned n);

    // Async write
    void startWrite(const Message& msg);
    void onWrite(const boost::system::error_code& ec);

    void handleError(const boost::system::error_code& /*ec*/);

    //
    tcp::socket socket_;

    std::string symbol_;
    ChronoTimepoint login_time_;
    std::function<void(std::shared_ptr<Session>)> sign_callback_;
    std::function<void(std::shared_ptr<Session>)> release_callback_;
    boost::asio::streambuf in_buffer_;

    std::string out_buffer_;

    std::mutex mtx_sending_;
    std::deque<Message> out_queue_;
    bool is_sending_{false};
};

using SessionPtr = std::shared_ptr<Session>;

class TcpServer
{
    using tcp = boost::asio::ip::tcp;

public:
    template <typename SignCallback, typename ReleaseCallback>
    TcpServer(boost::asio::io_context& ioc, unsigned short port,
              SignCallback&& cbIn, ReleaseCallback&& cbOut)

        : ioc_{ioc},
          port_{port},
          acceptor_(ioc, tcp::endpoint(tcp::v4(), port)),
          sign_callback_(std::forward<SignCallback>(cbIn)),
          release_callback_(std::forward<ReleaseCallback>(cbOut))
    {
    }

    void start();

private:
    //
    // Async accept
    void startAccept();
    void onAccept(std::shared_ptr<Session> session,
                  const boost::system::error_code& ec);

    //
    boost::asio::io_context& ioc_;
    unsigned short port_{};
    tcp::acceptor acceptor_;
    std::function<void(std::shared_ptr<Session>)> sign_callback_;
    std::function<void(std::shared_ptr<Session>)> release_callback_;
};

}  // namespace test

#endif
