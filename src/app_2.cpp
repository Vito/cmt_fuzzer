/////////////////////////////////////////////////////////////////////////////////
//
//  Daniel Petrovic, Dec/2020
//
//  File: app_2.cpp
//
/////////////////////////////////////////////////////////////////////////////////
#include "pch.hpp"

#include "app_2.hpp"
#include "utility.hpp"

namespace test
{
namespace mode2
{
App::App(const Settings& settings)

    : table_{settings.table},
      count_{settings.num_worker_threads},
      queue_{count_},
      ioc_{settings.ioc},
      server_{
          ioc_, settings.port,
          [self = this](std::shared_ptr<Session> s) { self->onSignClient(s); },
          [self = this](std::shared_ptr<Session> s) {
              self->onReleaseClient(s);
          }}
{
    // convert the HH:MM:SS:SSSSSS
    // to the timestamp of current day
    auto today = ChronoClock::now();
    timepoints_.reserve(table_.size());

    for (unsigned i = 0; i < table_.size(); ++i)
    {
        const auto& row = table_.row(i);
        timepoints_.push_back(asChronoTimepoint(row.ts, today));

        // cache the row id's corresponding to each symbol
        client_data_[row.sym].index_list.push_back(i);
    }

    for (unsigned n = 0; n < count_; ++n)
    {
        // start worker threads
        threads_.emplace_back([&, n]() {
            try
            {
                run(n);
            }
            catch (std::exception& e)
            {
                // for now just log error and terminate
                LogErr("%s", e.what());
                throw;
            }
        });

        setThreadName(threads_.back().native_handle(),
                      ("testApp_T_W" + std::to_string(n)).c_str());
    }
}

App::~App()
{
    for (auto& q : queue_) q.done();
    for (auto& t : threads_) t.join();
}

void App::run(const unsigned thread_index)
{
    //
    // "Worker" thread entry method.
    // Each thread pops the next session
    // to be processed and delivers update to
    // the clients.
    // The data are divided into chunks of fixed size
    // and delivered synchronously within thread.
    //

    using boost::asio::string_view;
    using IndexList = std::vector<unsigned>;

    // chunk size for sending via tcp
    constexpr unsigned kChunkSize = 1024;

    std::ostringstream row;
    std::string buf;
    string_view chunk;

    auto init_buffer = [&]() {
        row.str("");
        row.clear();
        buf.clear();
        buf.reserve(kChunkSize + kChunkSize / 10);
    };

    // Get next chunk given list of row indexes and starting point
    auto next_chunk = [&](const IndexList& v, unsigned& n) -> string_view {
        if (n >= v.size()) return string_view{};

        for (; n < v.size() && buf.size() < kChunkSize; ++n)
        {
            row.str("");
            row.clear();
            row << table_.row(v[n]) << '\n';
            buf += row.str();
        }

        return string_view(
            buf.data(),
            std::min(static_cast<unsigned>(buf.size()), kChunkSize));
    };

    // Mark chunk as consumed after being successfully sent
    auto consume_buf = [&](unsigned n) {
        n = std::min(n, static_cast<unsigned>(buf.size()));
        buf.erase(buf.begin(), buf.begin() + n);
    };

    while (true)
    {
        SCOPE_EXIT { init_buffer(); };

        SessionPtr session;

        for (unsigned i = 0; i < count_; ++i)
        {
            // try to pop next client from some thread's queue without locking
            // -> "task stealing" to avoid lock's at high load
            if (queue_[(i + thread_index) % count_].tryPop(session)) break;
        }

        // none found, so wait lock for the next
        if (!session && !queue_[thread_index].pop(session)) break;

        // now we got some client - find the rows and send
        auto c = clientData(session->symbol());

        if (c == nullptr) continue;

        // find first timpoint/row from table with timestamp >= the client login
        // time
        auto first = std::lower_bound(timepoints_.begin(), timepoints_.end(),
                                      session->loginTime());
        if (first == timepoints_.end()) continue;
        unsigned n0 = first - timepoints_.begin();
        unsigned n =
            std::lower_bound(c->index_list.begin(), c->index_list.end(), n0) -
            c->index_list.begin();

        Debug("thread_index=%u, n0= %u, n=%u", thread_index, n0, n);

        // get all rows for this client starting from now
        // serialize, divide into chunks and send
        boost::system::error_code ec;

        auto chunk = next_chunk(c->index_list, n);
        for (; !chunk.empty(); chunk = next_chunk(c->index_list, n))
        {
            auto len = session->sendSynch(chunk, ec);
            if (ec)
            {
                LogErr("sending to client failed: %s", ec.message().c_str());
                break;
            }
            else
            {
                consume_buf(len);
            }
        }
    }
}

void App::onSignClient(std::shared_ptr<Session> session)
{
    assert(session != nullptr);

    if (!clientData(session->symbol()))
    {
        LogErr("Client sent invalid symbol: '%s'", session->symbol().c_str());
        return;
    }

    // Try to push the new Session to some thread's queue
    // without locking
    for (unsigned i = 0; i < count_; ++i)
        if (queue_[(i + index_) % count_].tryPush(session)) return;

    // Otherwise wait lock
    index_ = (index_ + 1) % count_;
    queue_[index_].push(session);
}

void App::onReleaseClient(std::shared_ptr<Session> session)
{
    assert(session != nullptr);
    auto c = clientData(session->symbol());

    if (c == nullptr) return;

    {
        // std::unique_lock<std::mutex> lck{c->mtx};
        c->sessions.erase(
            std::remove(c->sessions.begin(), c->sessions.end(), session),
            c->sessions.end());
    }
}

ClientData* App::clientData(const std::string& symbol)
{
    auto it = client_data_.find(symbol);
    if (it == client_data_.end()) return nullptr;
    return &(it->second);
}

int App::run()
{
    LogInfo("Starting App in Mode 2");
    setThreadName(pthread_self(), "testApp_T_MAIN");

    server_.start();
    ioc_.run();
    return 0;
}

}  // namespace mode2

}  // namespace test
