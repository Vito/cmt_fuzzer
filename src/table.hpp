/////////////////////////////////////////////////////////////////////////////////
//
//  Daniel Petrovic, Dec/2020
//
//  File: table.hpp
//
//  Description:
//
//  Table like data structure for holding rows from .csv file
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef TEST_TABLE_HPP_
#define TEST_TABLE_HPP_

#include <chrono>
#include <cstdint>
#include <iosfwd>
#include <string>
#include <tuple>
#include <vector>

namespace test
{
struct Timestamp
{
    using ChronoTimepoint = std::chrono::system_clock::time_point;

    Timestamp() = default;

    Timestamp(unsigned h, unsigned m, unsigned s, unsigned mus) noexcept
        : hms{h, m, s, mus}
    {
    }

    Timestamp(const ChronoTimepoint& timepoint)
    {
        std::time_t t = std::chrono::system_clock::to_time_t(timepoint);
        struct std::tm ttm;
        localtime_r(&t, &ttm);
        std::get<0>(hms) = static_cast<uint8_t>(ttm.tm_hour);
        std::get<1>(hms) = static_cast<uint8_t>(ttm.tm_min);
        std::get<2>(hms) = static_cast<uint8_t>(ttm.tm_sec);
        std::get<3>(hms) = static_cast<uint32_t>(
            std::chrono::duration_cast<std::chrono::microseconds>(
                timepoint.time_since_epoch() % std::chrono::seconds(1))
                .count());
    }

    unsigned hours() const noexcept { return std::get<0>(hms); }
    unsigned minutes() const noexcept { return std::get<1>(hms); }
    unsigned seconds() const noexcept { return std::get<2>(hms); }
    unsigned museconds() const noexcept { return std::get<3>(hms); }

    bool operator<(const Timestamp& ts) const noexcept { return hms < ts.hms; }
    bool operator==(const Timestamp& ts) const noexcept
    {
        return hms == ts.hms;
    }
    bool operator!=(const Timestamp& ts) const noexcept
    {
        return !this->operator==(ts);
    }

    std::chrono::microseconds operator-(const Timestamp&);

private:
    //          hr        min       sec    musec
    std::tuple<uint8_t, uint8_t, uint8_t, uint32_t> hms;
};

std::chrono::system_clock::time_point asChronoTimepoint(
    const Timestamp& ts, const std::chrono::system_clock::time_point& now);

struct DataRow
{
    Timestamp ts;
    std::string sym;
    double price{};
};

struct Table
{
    const auto& row(std::size_t idx) const { return rows_[idx]; }
    auto size() const noexcept { return rows_.size(); }
    auto add(const DataRow& r) { rows_.push_back(r); }

    auto begin() const noexcept { return rows_.cbegin(); }
    auto begin() noexcept { return rows_.begin(); }

    auto end() const noexcept { return rows_.cend(); }
    auto end() noexcept { return rows_.end(); }

private:
    std::vector<DataRow> rows_;
};

std::istream& operator>>(std::istream&, Timestamp&);
std::ostream& operator<<(std::ostream&, const Timestamp&);

std::istream& operator>>(std::istream&, DataRow&);
std::ostream& operator<<(std::ostream&, const DataRow&);

void operator>>(std::istream&, Table&);
std::ostream& operator<<(std::ostream&, const Table&);

void skipComments(std::istream&);

}  // namespace test

#endif
