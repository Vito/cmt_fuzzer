/////////////////////////////////////////////////////////////////////////////////
//
//  Daniel Petrovic, Dec/2020
//
//  File: app_1.cpp
//
/////////////////////////////////////////////////////////////////////////////////
#include "pch.hpp"

#include "app_1.hpp"
#include "utility.hpp"

namespace test
{
namespace mode1
{
App::App(const Settings& settings)

    : table_{settings.table},
      is_alive_(table_.size(), 0),
      count_{settings.num_worker_threads},
      ioc_{settings.ioc},
      server_{
          ioc_, settings.port,
          [self = this](std::shared_ptr<Session> s) { self->onSignClient(s); },
          [self = this](std::shared_ptr<Session> s) {
              self->onReleaseClient(s);
          }}
{
    // convert the HH:MM:SS:SSSSSS
    // to the timestamp of current day
    auto today = ChronoClock::now();
    timepoints_.reserve(table_.size());

    for (unsigned i = 0; i < table_.size(); ++i)
    {
        const auto& row = table_.row(i);
        timepoints_.push_back(asChronoTimepoint(row.ts, today));

        // cache the row id's corresponding to each symbol
        client_data_[row.sym].index_list.push_back(i);
    }

    // start "worker" threads
    for (unsigned n = 0; n < count_; ++n)
    {
        threads_.emplace_back([&, n]() {
            try
            {
                run(n);
            }
            catch (std::exception& e)
            {
                // for now just log error and terminate
                LogErr("%s", e.what());
                throw;
            }
        });

        setThreadName(threads_.back().native_handle(),
                      ("testApp_T_W" + std::to_string(n)).c_str());
    }
}

App::~App()
{
    for (auto& t : threads_) t.join();
}

void App::run(const unsigned thread_index)
{
    //
    // "Worker" thread entry method.
    // Each thread waits for corresponding timestamp
    // and inform's the master thread to start
    // asynchronous write to the client.
    //
    // Thread with index <thread_index> handles
    // row index <n> if (<n> % count_) == (<thread_index> % count_)
    // where count_ = number of threads
    //

    auto trash = std::chrono::microseconds(0);

    static const auto N = timepoints_.size();

    auto next = timepoints_.begin();

    for (auto now = ChronoClock::now() - std::chrono::microseconds(1);;)
    {
        // increase current timestamp at least by 1 musec to prevent processing
        // same row twice
        now = std::max(now + std::chrono::microseconds(1), ChronoClock::now());
        next = std::lower_bound(next, timepoints_.end(), now);

        unsigned n = next - timepoints_.begin();

        //
        // Look for the next row starting from now
        // with n % count_ == thread_index
        while (n < N && n % count_ != thread_index) ++n;

        //
        // only intersted in rows with at least one active client
        while (n < N && is_alive_[n] == 0) n += count_;

        if (n >= N)
        {
            //
            // No row found. Wait a little and try again.
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
            next = timepoints_.begin();
            continue;
        }

        //
        // Next row found. Wait until it's timestamp(==target)
        // and trigger sending update to the client
        auto target = timepoints_[n];
        now = ChronoClock::now();

        // Wake up by <trash> value earlier.
        // Try to consider side effects (like thread context switching delay,
        // etc.) (on a RTOS <trash> should probably not be needed)
        while (now < target)
        {
            std::this_thread::sleep_for(target - now - trash);
            now = ChronoClock::now();
        }

        // Try to adjust the <trash> for the next iteration.
        // Add (err/2) to the <trash> and try to stable converge to some steady
        // value, if such exists. This will probably not be the case if the
        // system load varies frequently but for a fix system load I would
        // expect that the err -> 0-10 musec after some time (for ex. after
        // ~1min)
        auto err = now - target;
        trash += std::chrono::duration_cast<std::chrono::microseconds>(err / 2);

        Debug(
            "row/not/target/error(musec) = %u/%s/%s/%ld\n", n,
            asString(now).c_str(), asString(target).c_str(),
            std::chrono::duration_cast<std::chrono::microseconds>(err).count());

        processRow(now, n);
    }
}

void App::processRow(const ChronoTimepoint& now, unsigned n)
{
    const auto& row = table_.row(n);
    auto& c = client_data_[row.sym];
    Message msg({now, row.sym, row.price});
    for (auto& session : c.sessions) session->send(msg);
}

void App::onSignClient(std::shared_ptr<Session> session)
{
    assert(session != nullptr);

    Debug("(sym = %s)\n", session->symbol().c_str());

    auto c = clientData(session->symbol());

    if (c == nullptr)
    {
        LogErr("unknown symbol (%s)", session->symbol().c_str());
        return;
    }

    {
        // std::unique_lock<std::mutex> lck{c->mtx};
        c->sessions.erase(
            std::remove(c->sessions.begin(), c->sessions.end(), session),
            c->sessions.end());
        c->sessions.push_back(session);
    }

    {
        // std::unique_lock<std::mutex> lck{mtx_is_alive_};
        for (auto i : c->index_list) ++is_alive_[i];
    }
}

void App::onReleaseClient(std::shared_ptr<Session> session)
{
    assert(session != nullptr);
    auto c = clientData(session->symbol());

    if (c == nullptr) return;

    {
        // std::unique_lock<std::mutex> lck{c->mtx};
        c->sessions.erase(
            std::remove(c->sessions.begin(), c->sessions.end(), session),
            c->sessions.end());
    }

    {
        // std::unique_lock<std::mutex> lck{mtx_is_alive_};
        for (auto i : c->index_list) --is_alive_[i];
    }
}

ClientData* App::clientData(const std::string& symbol)
{
    auto it = client_data_.find(symbol);
    if (it == client_data_.end()) return nullptr;
    return &(it->second);
}

int App::run()
{
    LogInfo("Starting App in Mode 1");
    setThreadName(pthread_self(), "testApp_T_MAIN");
    server_.start();
    ioc_.run();
    return 0;
}

}  // namespace mode1

}  // namespace test
