/////////////////////////////////////////////////////////////////////////////////
//
//  Daniel Petrovic, Dec/2020
//
//  File: settings.hpp
//
//  Description:
//
//  Application settings data structure
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef TEST_SETTINGS_HPP_
#define TEST_SETTINGS_HPP_

#include "table.hpp"

namespace boost
{
namespace asio
{
class io_context;
}
}  // namespace boost

namespace test
{
struct Settings
{
    Table table;
    unsigned short port;
    unsigned num_worker_threads;
    boost::asio::io_context& ioc;
};

}  // namespace test

#endif
