#include "pch.hpp"

#include "tcp.hpp"
#include "utility.hpp"

namespace chrono = std::chrono;
namespace net = boost::asio;
namespace po = boost::program_options;
using tcp = net::ip::tcp;
using namespace test;

po::options_description createProgramOptions();

namespace po = boost::program_options;

class Reader
{
public:
    Reader(tcp::socket& s) : socket_{s} { startRead(); }

private:
    void startRead()
    {
        net::async_read_until(
            socket_, in_buffer_, '\n',
            boost::bind(&Reader::onRead, this, net::placeholders::error,
                        net::placeholders::bytes_transferred));
    }

    void onRead(const boost::system::error_code& ec, unsigned n)
    {
        if (ec)
        {
            LogErr("Reader::onRead(): %s", ec.message().c_str());
        }
        else
        {
            using net::buffers_begin;
            std::string line(buffers_begin(in_buffer_.data()),
                             buffers_begin(in_buffer_.data()) + n);
            std::istringstream data(line);

            Message msg;

            if (data >> msg)
            {
                std::cerr << msg << std::endl;
            }
            else
            {
                std::cerr << "Error: cannot deserialize input: " << line
                          << std::endl;
            }
            in_buffer_.consume(n);
            startRead();
        }
    }

    tcp::socket& socket_;
    net::streambuf in_buffer_;
};

int main(int argc, const char* argv[])
{
    auto opts = createProgramOptions();
    po::variables_map vars;
    po::store(po::parse_command_line(argc, argv, opts), vars);

    if (argc < 2 || vars.count("help") || !vars.count("symbol"))
    {
        std::cout << opts << std::endl;
        return 0;
    }

    auto symbol = vars["symbol"].as<std::string>();
    auto host = vars["host"].as<std::string>();
    auto port = vars["port"].as<unsigned short>();
    std::cerr << "starting client for symbol: " << symbol << '\n';
    std::cerr << "host = " << host << ", port = " << port << std::endl;

    net::io_context ioc;

    tcp::resolver resolver{ioc};
    tcp::socket socket{ioc};
    net::connect(socket, resolver.resolve(host, std::to_string(port)));

    boost::system::error_code ec;
    unsigned n{};

    {
        Message msg{std::chrono::system_clock::now(), {symbol}, {}};

        std::ostringstream data;
        data << msg << '\n';
        std::string buf = data.str();

        std::cout << "Client trying to send data: " << buf << std::endl;
        n = net::write(socket, net::buffer(buf), ec);

        if (ec)
        {
            std::cerr << "client error writing: " << ec.message() << std::endl;
        }
        else
        {
            std::cerr << n << " bytes written: " << data.str() << std::endl;
        }
    }

    Reader reader{socket};
    ioc.run();
}

po::options_description createProgramOptions()
{
    po::options_description opts("Program options");

    opts.add_options()("help", "print help")(
        "host", po::value<std::string>()->default_value("localhost"),
        "server address")("port",
                          po::value<unsigned short>()->default_value(27500),
                          "server port")(
        "symbol", po::value<std::string>()->required()->default_value(""),
        "symbol, for ex. AAL");

    return opts;
}
