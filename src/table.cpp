/////////////////////////////////////////////////////////////////////////////////
//
//  Daniel Petrovic, Dec/2020
//
//  File: table.cpp
//
//
/////////////////////////////////////////////////////////////////////////////////
#include "pch.hpp"

#include "table.hpp"

#include <chrono>
#include <iomanip>

namespace test
{
std::chrono::microseconds Timestamp::operator-(const Timestamp& t2)
{
    thread_local static auto musec = [](const Timestamp& t) {
        std::chrono::microseconds::rep n =
            t.hours() * 3600 + t.minutes() * 60 + t.seconds();

        return n * 1000'000 + t.museconds();
    };

    return std::chrono::microseconds{musec(*this) - musec(t2)};
}

std::chrono::system_clock::time_point asChronoTimepoint(
    const Timestamp& ts, const std::chrono::system_clock::time_point& now)
{
    std::time_t t = std::chrono::system_clock::to_time_t(now);

    struct std::tm ttm;
    localtime_r(&t, &ttm);

    ttm.tm_hour = ts.hours();
    ttm.tm_min = ts.minutes();
    ttm.tm_sec = ts.seconds();
    std::time_t tt = mktime(&ttm);

    if (tt == -1)
    {
        // TODO: handle error
        return now;
    }

    auto tp = std::chrono::system_clock::from_time_t(tt);
    tp += std::chrono::microseconds(ts.museconds());

    return tp;
}

std::istream& operator>>(std::istream& in, Timestamp& ts)
{
    unsigned int h, m, s, mus;
    in >> h;
    in.ignore();
    in >> m;
    in.ignore();
    in >> s;
    in.ignore();
    in >> mus;
    if (in) ts = Timestamp(h, m, s, mus);
    return in;
}

std::ostream& operator<<(std::ostream& os, const Timestamp& ts)
{
    os << std::right << std::setw(2) << std::setfill('0') << ts.hours() << ':'
       << std::right << std::setw(2) << ts.minutes() << ':' << std::right
       << std::setw(2) << ts.seconds() << '.' << std::left << std::setw(6)
       << std::setfill(' ') << ts.museconds();
    return os;
}

std::istream& operator>>(std::istream& in, DataRow& p)
{
    DataRow pp;
    in >> pp.ts >> pp.sym >> pp.price;
    if (in) p = pp;
    return in;
}

std::ostream& operator<<(std::ostream& os, const DataRow& p)
{
    os << p.ts << ' ' << p.sym << ' ' << p.price;
    return os;
}

void operator>>(std::istream& in, Table& t)
{
    struct CommaIsSpace : std::ctype<char>
    {
        CommaIsSpace() : std::ctype<char>(table()) {}

        static mask const* table()
        {
            static mask rc[table_size];
            rc[','] = std::ctype_base::space;
            rc[' '] = std::ctype_base::space;
            rc['\n'] = std::ctype_base::space;
            return &rc[0];
        }
    };

    skipComments(in);

    auto old_loc =
        std::cin.imbue(std::locale(std::cin.getloc(), new CommaIsSpace));

    for (DataRow r; in >> r;) t.add(r);

    // just to make sure rows are sorted by ts
    // probably not needed
    sort(t.begin(), t.end(),
         [](const DataRow& r1, const DataRow& r2) { return r1.ts < r2.ts; });

    std::cin.imbue(old_loc);
}

std::ostream& operator<<(std::ostream& os, const Table& t)
{
    for (const auto& row : t) os << row << '\n';
    return os;
}

void skipComments(std::istream& in)
{
    for (char c; (in >> std::skipws >> c) && (c == '#');)
        in.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

    if (in) in.unget();
}

}  // namespace test
