/////////////////////////////////////////////////////////////////////////////////
//
//  Daniel Petrovic, Dec/2020
//
//  File: tcp.cpp
//
//
/////////////////////////////////////////////////////////////////////////////////
#include "pch.hpp"

#include "tcp.hpp"

namespace test
{
void Session::start() { startRead(); }

void Session::startRead()
{
    boost::asio::async_read_until(
        socket_, in_buffer_, '\n',
        boost::bind(&Session::onRead, shared_from_this(),
                    boost::asio::placeholders::error,
                    boost::asio::placeholders::bytes_transferred));
}

void Session::onRead(const boost::system::error_code& ec, unsigned n)
{
    if (ec)
    {
        LogErr("Session::onRead(): %s", ec.message().c_str());
        handleError(ec);
        return;
    }

    try
    {
        using boost::asio::buffers_begin;

        std::string line(buffers_begin(in_buffer_.data()),
                         buffers_begin(in_buffer_.data()) + n);

        Message msg;
        std::istringstream data(line);
        data >> msg;
        in_buffer_.consume(n);

        if (!data)
        {
            LogErr("Could not parse received data. Data = '%s'",
                   line.c_str());
        }
        else
        {
            login_time_ = ChronoClock::now();
            symbol_ = msg.data.sym;

            Debug("Client signed up for symbol (%s). Now = %s",
                  msg.data.sym.c_str(), asString(login_time_).c_str());

            sign_callback_(shared_from_this());
        }

        startRead();
    }
    catch (const std::exception& e)
    {
        LogErr("%s", e.what());
        return;
    }
}

void Session::send(const Message& msg)
{
    std::unique_lock<std::mutex> lck{mtx_sending_};
    if (is_sending_)
    {
        out_queue_.push_back(msg);
    }
    else
    {
        startWrite(msg);
        is_sending_ = true;
    }
}

void Session::startWrite(const Message& msg)
{
    std::ostringstream data;
    data << msg << '\n';
    out_buffer_ = data.str();

    Debug("Server sending data: '%s'\n", out_buffer_.c_str());

    boost::asio::async_write(socket_, boost::asio::buffer(out_buffer_),
                             boost::bind(&Session::onWrite, shared_from_this(),
                                         boost::asio::placeholders::error));
}

void Session::onWrite(const boost::system::error_code& ec)
{
    if (ec)
    {
        LogErr("Server coud not write for symbol (%s): %s", symbol_.c_str(),
               ec.message().c_str());
        handleError(ec);
    }

    {
        std::unique_lock<std::mutex> lck{mtx_sending_};
        if (out_queue_.empty())
        {
            is_sending_ = false;
        }
        else
        {
            startWrite(out_queue_.front());
            out_queue_.pop_front();
            is_sending_ = true;
        }
    }
}

void Session::handleError(const boost::system::error_code& /*ec*/)
{
    release_callback_(shared_from_this());
}

// TcpServer
//
void TcpServer::start()
{
    LogInfo("Starting TCP server on port: %d", port_);
    startAccept();
}

void TcpServer::startAccept()
{
    auto session =
        std::make_shared<Session>(ioc_, sign_callback_, release_callback_);
    acceptor_.async_accept(session->socket(),
                           boost::bind(&TcpServer::onAccept, this, session,
                                       boost::asio::placeholders::error));
}

void TcpServer::onAccept(std::shared_ptr<Session> session,
              const boost::system::error_code& ec)
{
    Debug("new client connected");

    if (!ec)
    {
        session->start();
    }
    else
    {
        LogErr("accept failed: %s", ec.message().c_str());
    }

    startAccept();
}


}  // namespace test
