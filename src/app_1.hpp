/////////////////////////////////////////////////////////////////////////////////
//
//  Daniel Petrovic, Dec/2020
//
//  File: app_1.hpp
//
//  Description:
//
//  Header file for Mode 1 App
//  test::mode1::App
//  ->sends updates to the clients
//  according to the current system timestamp and the .csv data.
//  Single master and multiple "worker" threads are spawn.
//
//  The master thread entry function is run().
//  The master thread receives incoming TCP connections
//  and sends write requests to the clients asynchronously.
//  The data to be sent are coming from the worker threads.
//
//  The worker thread(s) entry function is run(thread_index).
//  Each worker thread is responsible for waiting on the
//  n.th timestamp from the .csv file, having: n % num of threads = thread_id
//
//  The assumption is that the main goal is to "catch" the system timestamp
//  as accurate as possible according the .csv data (microsecond precision).
//  For this to work, it seems advisable to keep the number of threads rather
//  low because of delays due context switches, etc.
//
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef TEST_APP1_H_
#define TEST_APP1_H_

#include "tcp.hpp"
#include "settings.hpp"
#include "table.hpp"

#include <algorithm>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <deque>
#include <map>
#include <memory>
#include <mutex>
#include <thread>
#include <unordered_map>

#include <boost/asio.hpp>
#include <boost/asio/io_context.hpp>

namespace test
{
namespace net = boost::asio;
using tcp = net::ip::tcp;

namespace mode1
{
struct ClientData
{
    std::mutex mtx;
    std::vector<unsigned> index_list;  // indexes to table rows with this symbol
    std::vector<std::shared_ptr<Session>> sessions;
};

class App
{
    using ChronoClock = std::chrono::system_clock;
    using ChronoTimepoint = ChronoClock::time_point;

public:
    //
    App(const Settings&);
    ~App();

    //
    // Entry method for the master thread
    int run();

private:
    //
    // Entry method for "worker" threads
    void run(unsigned n);

    //
    // Invoked by the worker thread(s) when timestamp reached
    // the target value.
    void processRow(const ChronoTimepoint& now, unsigned row);

    //
    // Executed on master thread when new client signed
    void onSignClient(std::shared_ptr<Session> session);

    //
    // Executed on master when client is to be removed from
    // from the update list
    void onReleaseClient(std::shared_ptr<Session> session);

    //
    // We are holding a lookup-map with some precalculated values
    // and active sessions for each known symbol from the .csv file.
    ClientData* clientData(const std::string& symbol);

    //
    // Stores the data read from the .csv file
    const Table& table_;

    //
    // Holds precalculated values of the HH:MM:SS.SSSSSS
    // to the system timepoint (std::chrono::system_clock::time_point)
    // for faster lookups.
    std::vector<ChronoTimepoint> timepoints_;

    //
    // Number of active clients (sessions) for
    // each row in the .csv table.
    std::vector<int> is_alive_;

    // for now only 1 thread responsible for
    // writing, so no mutex needed
    // std::mutex mtx_is_alive_;

    //
    // Number of spawn "worker" threads.
    const unsigned count_;

    //
    // "Worker" threads
    std::vector<std::thread> threads_;

    //
    // Lookup-map for precalculated data and active
    // sessions for each symbol
    std::unordered_map<std::string, ClientData> client_data_;

    boost::asio::io_context& ioc_;
    TcpServer server_;
};

}  // namespace mode1
}  // namespace test

#endif
