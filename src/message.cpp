/////////////////////////////////////////////////////////////////////////////////
//
//  Daniel Petrovic, Dec/2020
//
//  File: message.cpp
//
/////////////////////////////////////////////////////////////////////////////////
#include "pch.hpp"

#include "message.hpp"

namespace test
{
std::ostream& operator<<(std::ostream& os, const Message& m)
{
    return os << m.data;
}

std::istream& operator>>(std::istream& is, Message& m) { return is >> m.data; }

}  // namespace test
