/////////////////////////////////////////////////////////////////////////////////
//
//  Daniel Petrovic, Dec/2020
//
//  File: app_2.hpp
//
//  Description:
//
//  Header file for Mode 2 App
//  test::mode2::App
//  -> sends all updates(rows) to the client at once
//     as fast as possible
//     except those with timestamp < now
//
//  The master thread entry function is run().
//  The master thread receives incoming TCP connections
//  and pushes those client session (Session) to a first
//  available thread's queue.
//
//  The worker thread(s) entry function is run(thread_index).
//  The worker thread(s) look for the first available Session queue
//  to pop a Session and deliever updates to those client.
//  There is a Session queue per each thread, but thread's are allowed
//  to pop from other thread's queue's as well -> task stealing.
//
//
/////////////////////////////////////////////////////////////////////////////////
#ifndef TEST_APP2_H_
#define TEST_APP2_H_

#include "notification_queue.hpp"
#include "tcp.hpp"
#include "settings.hpp"
#include "table.hpp"

#include <algorithm>
#include <atomic>
#include <chrono>
#include <condition_variable>
#include <deque>
#include <map>
#include <memory>
#include <mutex>
#include <thread>
#include <unordered_map>

#include <boost/asio.hpp>
#include <boost/asio/io_context.hpp>

namespace test
{
namespace net = boost::asio;
using tcp = net::ip::tcp;

namespace mode2
{
struct ClientData
{
    std::mutex mtx;
    std::vector<unsigned> index_list;  // indexes to table rows with this symbol
    std::vector<std::shared_ptr<Session>> sessions;
};

class App
{
    using ChronoClock = std::chrono::system_clock;
    using ChronoTimepoint = ChronoClock::time_point;

public:
    //
    App(const Settings&);
    ~App();

    //
    // Entry method for the master thread
    int run();

private:
    //
    // Entry method for "worker" threads
    void run(unsigned n);

    //
    // Executed on master thread when new client signed
    void onSignClient(std::shared_ptr<Session> session);

    //
    // Executed on master when client is to be removed from
    // from the update list
    void onReleaseClient(std::shared_ptr<Session> session);

    //
    // We are holding a lookup-map with some precalculated values
    // and active sessions for each known symbol from the .csv file.
    ClientData* clientData(const std::string& symbol);

    //
    // Stores the data read from the .csv file
    const Table& table_;

    //
    // Holds precalculated values of the HH:MM:SS.SSSSSS
    // to the system timepoint (std::chrono::system_clock::time_point)
    // for faster lookups.
    std::vector<ChronoTimepoint> timepoints_;

    //
    // Number of spawn "worker" threads.
    const unsigned count_;

    //
    // "Worker" threads
    std::vector<std::thread> threads_;

    //
    // Queue with active sessions to
    // be processed.
    // There is a queue per each thread.
    std::vector<NotifcationQueue<SessionPtr>> queue_;

    //
    // Start index for Round-Robin of the next
    // Queue to be popped from
    unsigned index_{0};

    //
    // Lookup-map for precalculated data and active
    // sessions for each symbol
    std::unordered_map<std::string, ClientData> client_data_;

    boost::asio::io_context& ioc_;
    TcpServer server_;
};

}  // namespace mode2
}  // namespace test

#endif
