/////////////////////////////////////////////////////////////////////////////////
//
//  Daniel Petrovic, Dec/2020
//
//  File: server.cpp
//
//  Description:
//
//  Server main file
//
/////////////////////////////////////////////////////////////////////////////////
#include "pch.hpp"

#include "app_1.hpp"
#include "app_2.hpp"
#include "table.hpp"
#include "utility.hpp"

namespace po = boost::program_options;

using AppMode1 = test::mode1::App;
using AppMode2 = test::mode2::App;

po::options_description createProgramOptions(unsigned max_threads);

int readMode();
test::Table readTable();

struct InvalidModeException : public std::invalid_argument
{
    using std::invalid_argument::invalid_argument;
};

int main(int argc, const char* argv[])
{
    using namespace test;

    try
    {
        const auto max_threads =
            std::max(1u, std::thread::hardware_concurrency() - 1);

        auto opts = createProgramOptions(max_threads);
        po::variables_map vars;
        po::store(po::parse_command_line(argc, argv, opts), vars);

        if (vars.count("help"))
        {
            std::cout << "Usage: " << argv[0]
                      << " [options] < input_file.csv\n\n";
            std::cout << opts << std::endl;
            return 0;
        }

        boost::asio::io_context ioc;

        Settings settings{readTable(), vars["port"].as<unsigned short>(),
                          std::min(max_threads, vars["j"].as<unsigned>()), ioc};

        {
        std::ofstream os("out.csv");
        os << settings.table;
        }

        return readMode() == 1 ? AppMode1(settings).run()
                               : AppMode2(settings).run();
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what() << std::endl;
        return 0;
    }
}

po::options_description createProgramOptions(unsigned max_threads)
{
    std::cout << "The server reads data from standard input\n\n";

    std::ostringstream desc_threads;
    desc_threads << "Number of worker threads (max = " << max_threads << ")";

    po::options_description opts("Program options");

    opts.add_options()("help", "print help")(
        "port", po::value<unsigned short>()->default_value(27500),
        "listener tcp port")(
        "j",
        po::value<unsigned>()->default_value(std::max(1u, max_threads / 2)),
        desc_threads.str().c_str());

    return opts;
}

int readMode()
{
    int mode = 1;
    auto env_mode = std::getenv("TEST_APP_MODE");

    try
    {
        if (env_mode) mode = std::stoi(env_mode);

        if (mode != 1 && mode != 2)
        {
            std::ostringstream err;
            err << "Invalid mode for TEST_APP_MODE: '" << env_mode << "'."
                << " Valid values: {1, 2}";

            throw InvalidModeException(err.str());
        }
    }
    catch (const std::exception& e)
    {
        LogErr("%s", e.what());
        throw;
    }

    return mode;
}

test::Table readTable()
{
    std::ios_base::sync_with_stdio(false);
    auto old_tie = std::cin.tie(nullptr);
    test::Table t;
    std::cin >> t;
    std::cin.tie(old_tie);
    return t;
}
