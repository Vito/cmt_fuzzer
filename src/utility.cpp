/////////////////////////////////////////////////////////////////////////////////
//
//  Daniel Petrovic, Dec/2020
//
//  File: utility.cpp
//
//
/////////////////////////////////////////////////////////////////////////////////
#include "pch.hpp"

#include "table.hpp"
#include "utility.hpp"

namespace test
{
std::string asString(const std::chrono::system_clock::time_point& tp)
{
    std::ostringstream s;
    Timestamp ts(tp);
    s << ts;
    return s.str();
}

}  // namespace test
