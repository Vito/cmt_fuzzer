#include "pch.hpp"

#include "table.hpp"
#include "utility.hpp"

using namespace test;

int main(int, const char*[])
{
    std::ios_base::sync_with_stdio(false);
    auto old_tie = std::cin.tie(nullptr);

    Table t;
    std::cin >> t;

    std::cin.tie(old_tie);

    std::time_t tt =
        std::chrono::system_clock::to_time_t(std::chrono::system_clock::now());
    struct std::tm tm;
    localtime_r(&tt, &tm);

    Table t2(t);
    for (auto& r : t2)
    {
        auto ts = r.ts;
        auto h = (ts.hours() - 20 + tm.tm_hour) % 24;
        auto m = (ts.minutes)() - 38 + tm.tm_min;
        if (m > 59)
        {
            m = 0;
            h = (h + 1) % 24;
        }
        auto sec = ts.seconds();
        auto musec = ts.museconds();

        r.ts = Timestamp(h, m, sec, musec);
    }
    std::cout << t2 << std::endl;
}
