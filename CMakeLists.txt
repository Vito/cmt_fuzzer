cmake_minimum_required (VERSION 3.16)

project (test_market)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

option(USE_PCH "Use precomiled headers" ON)
option(USE_CONAN "Use conan package manager" OFF)
option(USE_DEBUG "Enable extra debugging information" OFF)

if (USE_CONAN)
    include(${CMAKE_BINARY_DIR}/conanbuildinfo.cmake)
    conan_basic_setup()
endif()

add_subdirectory(src)
